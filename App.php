<?php

class App{
    public $a, $b, $c, $p, $triangle;
    public $points = Array();

    public function App($triangleWidth){
        $this->a = new Vector(0, 0);
        $this->b = new Vector($triangleWidth, 0);
        $this->c = new Vector($triangleWidth/2, $triangleWidth);
        $this->p = new Vector($triangleWidth/4, $triangleWidth/4);
        $this->triangle = [$this->a, $this->b, $this->c];

    }

    public function generateNext(Vector $pn){
        $corner = $this->triangle[$this->getRandomCorner()];
        $line = $corner->subtract($pn);
        $this->p = $line->divide(2);
    }

    private function getRandomCorner(){
        return array_rand($this->triangle);
    }
}
<?php

class Vector {

    private $x;
    private $y;

    public function Vector($x, $y){
        $this->x = $x;
        $this->y = $y;
    }

    public function getX(){
        return $this->x;
    }

    public function getY(){
        return $this->y;
    }

    public function sum(Vector $sum){
        return new Vector(($this->x + $sum->x), ($this->y + $sum->y));
    }

    public function subtract(Vector $subtract){
        return new Vector(($this->x - $subtract->x), ($this->y - $subtract->y));
    }

    public function multiply($factor){
        return new Vector(($this->x * $factor), ($this->y * $factor));
    }

    public function divide($divisor){
        return new Vector(($this->x / $divisor), ($this->y / $divisor));
    }
    public function printCoordinates(){
        echo "<br />X: $this->x";
        echo "<br />Y: $this->y";
    }

    public static function generateRandomCoordinate($limit){
        return rand($limit*-1, $limit);
    }
}
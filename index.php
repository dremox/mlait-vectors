<?php
ini_set('memory_limit', '-1');
include_once "Vector.php";
include_once "App.php";
$size = 1000;
$points = 50000;
$app = new App($size);

$im = imagecreate($size, $size);

$background_color = imagecolorallocate($im, 255, 255, 255);
$color = imagecolorallocate($im, 0, 161, 255);

for($i = 0; $i < $points; $i++){
    imagesetpixel($im, $app->p->getX(), $app->p->getY(), $color);
    $app->generateNext($app->p);
}
header("Content-Type: image/jpeg");

imagejpeg($im);


?>